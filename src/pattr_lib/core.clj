(ns pattr-lib.core)

(defn pattr-series
  "Generates a list from arithmetic series pattern"
  [start step length]
  (letfn [(gen-series
            [start step]
            (cons start (lazy-seq (gen-series (+ start step) step))))]
    (take length (gen-series start step))))

(defn pattr-geom
  "Generates a list from geometric series pattern"
  [start step length]
  (letfn [(gen-geom
            [start step]
            (cons start (lazy-seq (gen-geom (* start step) step))))]
    (take length (gen-geom start step))))

(defn pattr-ser
  "Generates list by selecting elements from pattern list N times"
  [ls ln]
  (take ln (cycle ls)))

(defn pattr-seq
  "Generates a list by repeating pattern list N times"
  [ls ln]
  (take (* ln (count ls)) (cycle ls)))

(defn pattr-collect
  "Generates a list by filtering pattern list using a function
  
  (pattr-collect (fn [x] (> x 2)) '(1 2 3 4 5))"
  [fn ls]
  (filter fn ls))

(defn pattr-reject
  "Generates a list by rejecting items from pattern list using a function"
  [fn-rej ls]
  (filter (fn [x] (not (fn-rej x))) ls))

(defn flat-list
  "Transforms nested list into flat list
  
  (flat-list (list (pattr-seq '(2 23 4) 2) (pattr-geom 4 2 23)))"
  [ls]
  (letfn [(rec-flat [rec-list ls] 
            (cond (empty? rec-list) ls
                  (seq? (first rec-list)) (rec-flat (rest rec-list) 
                                                     (rec-flat (first rec-list) ls))
                  (not (seq? (first rec-list))) (rec-flat (rest rec-list) 
                                                           (cons (first rec-list) ls))))]
    (cond (seq? ls) (reverse (rec-flat ls '()))
          :else (list ls))))
